const btn = document.querySelector('button');
let interval;
let secondes = 10;

function start (){

    interval = setInterval(counter, 1000);
}

function counter(){
    secondes--;
        if (secondes == 0) {
            stop()
        }else {
            document.body.innerHTML += secondes + '<br>';
        }
}

function stop() {
    clearInterval(interval);
    document.body.innerHTML += "Stop !";
}


btn.addEventListener('click', start());